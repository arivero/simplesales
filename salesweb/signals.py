from django.dispatch import receiver
from django.db.models.signals import post_save
from models import StockMovement, ItemStock, StockAdd, StockSubtract, DepositTransfer, Sale


class StockActions():

    @classmethod
    def update_stock(cls, stock_movement):
        item_stock_arr = ItemStock.objects.filter(item=stock_movement.item)

        try:
            item_stock = item_stock_arr.get(deposit=stock_movement.deposit)

        except ItemStock.DoesNotExist:
            item_stock = ItemStock(item=stock_movement.item, deposit=stock_movement.deposit, quantity=0)

        print item_stock
        item_stock.quantity += stock_movement.quantity * stock_movement.MULTIPLIER
        item_stock.save()

    @staticmethod
    @receiver(post_save, sender=StockAdd)
    def update_stock_add(sender, **kwargs):
        stock_add = kwargs['instance']
        StockActions.update_stock(stock_add)

    @staticmethod
    @receiver(post_save, sender=StockSubtract)
    def update_stock_sub(sender, **kwargs):
        stock_sub = kwargs['instance']
        StockActions.update_stock(stock_sub)

    @staticmethod
    @receiver(post_save, sender=Sale)
    def update_stock_sale(sender, **kwargs):
        sale = kwargs['instance']
        for sale_detail in sale.saledetail_set.all():
            sub = StockSubtract.create_from_sale_detail(sale_detail)
            sub.save()

    @staticmethod
    @receiver(post_save, sender=DepositTransfer)
    def do_transfer(sender, **kwargs):

        deposit_transfer = kwargs['instance']
        add = StockAdd.create_from_transfer(deposit_transfer)
        add.save()
        sub = StockSubtract.create_from_transfer(deposit_transfer)
        sub.save()

