from django.contrib import admin
from salesweb.models import Item, Customer, ItemStock, Deposit, DepositTransfer, Sale, SaleDetail, \
    StockAdd, StockSubtract, StockMovement

admin.site.site_header = "Sell it!"
admin.site.site_title = "Sell it admin"


# Register your models here.
admin.site.register(Item)
admin.site.register(Customer)
admin.site.register(ItemStock)
admin.site.register(Deposit)
admin.site.register(DepositTransfer)


class SaleDetailInLine(admin.TabularInline):
    model = SaleDetail

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    inlines = [SaleDetailInLine]


admin.site.register(StockAdd)
admin.site.register(StockSubtract)
admin.site.register(StockMovement)