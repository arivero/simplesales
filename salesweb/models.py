from django.db import models
from decimal import *
from datetime import *
# Create your models here.


class Item(models.Model):
    name = models.CharField(max_length=40)
    default_price = models.DecimalField(max_digits=6, decimal_places=2, default=50)

    def __unicode__(self):
        return self.name


class Deposit(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name


class ItemStock(models.Model):
    deposit = models.ForeignKey(Deposit)
    item = models.ForeignKey(Item)
    quantity = models.PositiveIntegerField()
    
    def __unicode__(self):
        return unicode(self.item) + " - " + unicode(self.deposit) + "(" + unicode(self.quantity) + ")"


class StockMovement(models.Model):
    deposit = models.ForeignKey(Deposit)
    item = models.ForeignKey(Item)
    quantity = models.PositiveIntegerField()

    def __unicode__(self):

        try:
            content_type = self.stockadd
        except StockAdd.DoesNotExist:
            try:
                content_type = self.stocksubtract
            except StockSubtract.DoesNotExist:
                content_type = "Unknown"

        return unicode(content_type) + " - " + unicode(self.deposit)


class StockAdd(StockMovement):
    PURCHASE = 1
    RETURN = 2
    INVENTORY = 3
    ROLLBACK = 4
    TRANSFER = 8
    OTHER = 9

    ADD_REASONS = (
        (PURCHASE, "Purchase"),
        (RETURN, "Return"),
        (INVENTORY, "Inventory"),
        (ROLLBACK, "Rollback"),
        (TRANSFER, "Transfer"),
        (OTHER, "Other")
    )

    MULTIPLIER = 1

    reason = models.PositiveSmallIntegerField(choices=ADD_REASONS, default=PURCHASE)

    def __unicode__(self):
        return unicode(self.item) + " - " + unicode(self.reason) + " - " + unicode(self.quantity)

    @staticmethod
    def create_from_transfer(transfer):
        s = StockAdd(
            reason=StockAdd.TRANSFER,
            deposit=transfer.to_deposit,
            item=transfer.item,
            quantity=transfer.quantity
        )
        return s


class DepositTransfer(StockMovement):

    to_deposit = models.ForeignKey(Deposit, related_name="trans_to_deposit")

    def __unicode__(self):
        return unicode(self.item) + ", from " + unicode(self.deposit) + " to " + unicode(self.to_deposit) + "(" + unicode(self.quantity) + ")"


class StockSubtract(StockMovement):
    SALE = 1
    RETURN = 2
    INVENTORY = 3
    ROLLBACK = 4
    LOSS = 5
    BREAKAGE = 6
    TRANSFER = 8
    OTHER = 9


    ADD_REASONS = (
        (SALE, "Sale"),
        (RETURN, "Return"),
        (INVENTORY, "Inventory"),
        (ROLLBACK, "Rollback"),
        (LOSS, "Loss"),
        (BREAKAGE, "Breakage"),
        (TRANSFER, "Transfer"),
        (OTHER, "Other")
    )

    MULTIPLIER = -1

    reason = models.PositiveSmallIntegerField(choices=ADD_REASONS, default=SALE)

    def __unicode__(self):
        return unicode(self.item) + " - " + unicode(self.reason) + " - " + unicode(self.quantity)

    @staticmethod
    def create_from_transfer(transfer):
        s = StockSubtract(
            reason=StockSubtract.TRANSFER,
            deposit=transfer.deposit,
            item=transfer.item,
            quantity=transfer.quantity
        )
        return s

    @staticmethod
    def create_from_sale_detail(saledetail):
        s = StockSubtract(
            reason=StockSubtract.SALE,
            deposit=saledetail.deposit,
            item=saledetail.item,
            quantity=saledetail.quantity,
        )
        return s


class Customer(models.Model):
    name = models.CharField(max_length=40)
    
    def __unicode__(self):
        return self.name
    
    
class Sale(models.Model):
    date = models.DateField()
    customer = models.ForeignKey(Customer)
    
    def __unicode__(self):
        return unicode(self.date) + "|" + unicode(self.customer) #TODO: Add total


class SaleDetail(models.Model):
    item = models.ForeignKey(Item)
    deposit = models.ForeignKey(Deposit)
    quantity = models.PositiveIntegerField()
    unit_price = models.DecimalField(max_digits=6, decimal_places=2)
    total = Decimal()
    sale = models.ForeignKey(Sale)

