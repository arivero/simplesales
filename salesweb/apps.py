from django.apps import AppConfig


class SalesConfig(AppConfig):
    name = "salesweb"
    verbose_name = "Stock and Sales management"

    def ready(self):
        import signals
